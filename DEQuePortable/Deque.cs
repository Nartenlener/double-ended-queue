﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DEQuePortable
{
    public sealed class Deque<T> : IQueueOperation<T>, IEnumerable<T>
    {
        private T[] collection;
        private T[] def;

        public Deque()
        {
            collection = new T[] { };
            def = new T[] { };
        }

        public IEnumerator<T> GetEnumerator()
        {
            return collection.Cast<T>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return collection.GetEnumerator();
        }

        /// <summary>
        /// Add item to end of the collection
        /// <para> e.g [A,B,C,D] -> AddLast(E) -> [A,B,C,D,E] </para>
        /// </summary>
        /// <param name="item"></param>
        public void AddLast(T item)
        {
            int size = collection.Length;
            T[] newCollection = new T[size + 1];

            Array.Copy(collection, 0, newCollection, 0, size);

            newCollection[size] = item;
            collection = newCollection;
        }

        /// <summary>
        /// Add item to beginning of the collection
        /// <para> e.g [A,B,C,D] -> AddFirst(E) -> [E,A,B,C,D] </para>
        /// </summary>
        /// <param name="item"></param>
        public void AddFirst(T item)
        {
            int size = collection.Length;
            T[] newCollection = new T[size + 1];

            Array.Copy(collection, 0, newCollection, 1, size);

            newCollection[0] = item;
            collection = newCollection;
        }

        /// <summary>
        /// Add item to the selected place in collection
        /// <para> e.g [A,B,C,D] -> AddPosition(E,1) -> [A,E,B,C,D] </para>
        /// </summary>
        /// <param name="item"></param>
        /// <param name="position"></param>
        public void AddPosition(T item, int position)
        {
            int correction = 0;
            int size = collection.Length;
            T[] newCollection = new T[size + 1];

            for(int i = 0; i < collection.Length; i++)
            {
                if(i.Equals(position))
                {
                    newCollection[i] = item;
                    newCollection[i + 1] = collection[i];
                    correction++;
                }
                else
                {
                    newCollection[i + correction] = collection[i];
                }
            }

            collection = newCollection;
        }

        /// <summary>
        /// Get first object from collection
        /// <para> e.g [A,B,C,D] -> GetFirst() -> return A </para>
        /// </summary>
        public T GetFirst()
        {
            int size = collection.Length;
            T obj = collection.FirstOrDefault();
            T[] newCollection = new T[size - 1];

            Array.Copy(collection, 1, newCollection, 0, size - 1);

            collection = newCollection;
            return obj;
        }

        /// <summary>
        /// Get last object from collection
        /// <para> e.g [A,B,C,D] -> GetFirst() -> return D </para>
        /// </summary>
        /// <returns></returns>
        public T GetLast()
        {
            int size = collection.Length;
            T obj = collection.LastOrDefault();
            T[] newCollection = new T[size - 1];

            Array.Copy(collection, 0, newCollection, 0, size - 1);

            collection = newCollection;
            return obj;
        }

        /// <summary>
        /// Get selected object from collection
        /// <para> e.g [A,B,C,D] -> GetFirst(1) -> return B </para>
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public T GetItem(int position)
        {
            int correction = 0;
            int size = collection.Length;
            T obj = default(T);

            if (size - 1 > 0)
            {
                T[] newCollection = new T[size - 1];
                
                for (int i = 0; i < collection.Length; i++)
                {
                    if (i.Equals(position))
                    {
                        obj = collection[i];
                        newCollection[i] = collection[i];
                        correction--;
                    }
                    else
                    {
                        newCollection[i + correction] = collection[i];
                    }
                }
                collection = newCollection;
            }
            else
            {
                obj = collection[position];
                collection = default(T[]);
            }

            return obj;
        }

        /// <summary>
        /// Get entire collection
        /// <para> e.g [A,B,C,D] -> GetAll() -> return [A,B,C,D] </para>
        /// </summary>
        /// <returns></returns>
        public T[] GetAll()
        {
            return collection;
        }

        /// <summary>
        /// Preview of the first element from collection
        /// <para> e.g [A,B,C,D] -> PeekFirst() -> return A </para>
        /// </summary>
        /// <returns></returns>
        public T PeekFirst()
        {
            return collection.FirstOrDefault();
        }

        /// <summary>
        /// Get selected object from collection
        /// <para> e.g [A,B,C,D] -> PeekLast() -> return D </para>
        /// </summary>
        /// <returns></returns>
        public T PeekLast()
        {
            return collection.LastOrDefault();
        }

        /// <summary>
        /// Get selected object from collection
        /// <para> e.g [A,B,C,D] -> PeekPosition(1) -> return B </para>
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T PeekPosition(int index)
        {
            return collection[index];
        }

        /// <summary>
        /// Delete first item from collection
        /// <para> e.g [A,B,C,D] -> RemoveFirst() -> [B,C,D] </para>
        /// </summary>
        public void RemoveFirst()
        {
            int size = collection.Length;
            T[] newCollection = new T[size - 1];

            for (int i = 1; i < collection.Length; i++)
            {
                newCollection[i - 1] = collection[i];
            }

            collection = newCollection;
        }

        /// <summary>
        /// Delete last item from collection
        /// <para> e.g [A,B,C,D] -> RemoveLast() -> [A,B,C] </para>
        /// </summary>
        public void RemoveLast()
        {
            int size = collection.Length;
            T[] newCollection = new T[size - 1];

            for (int i = 0; i < collection.Length - 1; i++)
            {
                newCollection[i] = collection[i];
            }

            collection = newCollection;
        }

        /// <summary>
        /// Delete selected item from collection
        /// <para> e.g [A,B,C,D] -> RemovePosition(1) -> [A,C,D] </para>
        /// </summary>
        /// <param name="position"></param>
        public void RemovePosition(int position)
        {
            int correction = 0;
            int size = collection.Length;
            T[] newCollection = new T[size - 1];

            for (int i = 0; i < collection.Length; i++)
            {
                if (i.Equals(position))
                {
                    correction = - 1;
                }
                else
                {
                    newCollection[i + correction] = collection[i];
                }
            }

            collection = newCollection;
        }

        /// <summary>
        /// Number of element in collection
        /// <para> e.g [A,B,C,D] -> Count() -> 4 </para>
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            return collection.Count();
        }

        /// <summary>
        /// Check if collection contains any element from input parameters
        /// <para> e.g [A,B,C,D] -> ContainsAny(E,F,A) -> true </para>
        /// <para>from: http://stackoverflow.com/a/195628/6883975 </para>
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        public bool ContainsAny(params T[] objects)
        {
            if (objects.Length > 0)
            {
                foreach (T value in objects)
                {
                    if (collection.Contains(value))
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Clear current collection
        /// </summary>
        /// <returns></returns>
        public void ClearAll()
        {
            collection = def;
        }
    }
}
