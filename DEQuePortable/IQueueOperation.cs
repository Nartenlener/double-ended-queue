﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEQuePortable
{
    interface IQueueOperation <T>
    {
        void AddFirst(T itemm);
        void AddLast(T item);
        void AddPosition(T item, int position);

        T GetItem(int position);
        T GetFirst();
        T GetLast();
        T[] GetAll();

        T PeekFirst();
        T PeekLast();
        T PeekPosition(int position);

        void RemoveFirst();
        void RemoveLast();
        void RemovePosition(int position);

        int Count();
        bool ContainsAny(params T[] obj);

        void ClearAll();
    }
}
