## Disclaimer ##

DEQuePortable is a first library created from me for my needs. It works but is not very efficient. If you don't put inside Deque more than 10000 object - good for you, you will not notice that something takes longer than normal.

Remembr: I am not professional programmer and my code is not brilliant, but I learn day by day :)

## Current Version:
**1.0.0 (stable)**

## Available methods ##
```
#!csharp

    public sealed class Deque<T> : IQueueOperation<T>, IEnumerable<T>
    {
        public Deque()

        public void AddFirst(T item);
        public void AddLast(T item);
        public void AddPosition(T item, int position);

        public T GetItem(int position)
        public T GetFirst()
        public T GetLast()
        public T[] GetAll()

        public T PeekFirst()
        public T PeekLast()
        public T PeekPosition(int position)

        public void RemoveFirst()
        public void RemoveLast()
        public void RemovePosition(int position)

        public int Count()
        public bool ContainsAny(params T[] objects)
    }
```

### Do you have questions? ###
mail me: morrowind1[a}wp.pl